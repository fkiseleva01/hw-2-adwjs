const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];



const tegDiv = document.createElement("div");
tegDiv.id = "root";
document.body.appendChild(tegDiv);

const div = document.querySelector('#root');

let needInfo = ['author','name','price'];

books.forEach((book) => {
  try{
      needInfo.forEach((prop) => {
        if(!book[prop]) {
          throw new Error(`This book dont have ${prop}`);
        }
      }) 

      const ul = document.createElement('ul');
        for (prop in book) {
          const li = document.createElement('li');
          li.innerText = `${prop}: ${book[prop]}`
          ul.append(li)
        }
        div.append(ul)
      }
  catch(error) {  
      console.log(error)
  }
})
